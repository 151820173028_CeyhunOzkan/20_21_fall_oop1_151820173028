#include <iostream>
using namespace std;
/*!
 * @mainpage
 * @author 151820173028 , Ceyhun OZKAN ,OOP-1 LAB Homework-2
 * @date 09/11/2020
 * \brief It prints the numbers sent in reverse with array
 */
int main()
{
    int arr[1000], size;
    cin >> size;
    for (int i = 0; i < size; i++)
        cin >> arr[i];
    for (int i = size - 1; i >= 0; i--)
        cout << arr[i] << " ";
    return 0;
}