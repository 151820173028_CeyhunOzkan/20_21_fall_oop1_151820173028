#include <iostream>
using namespace std;
/*!
 * @mainpage
 * @author 151820173028 , Ceyhun OZKAN ,OOP-1 LAB Homework-2
 * @date 09/11/2020
 * \brief It finds the largest of the 4 numbers sent
 */
 /*!
 * \brief Function finds the largest of the 4 numbers sent
 *@param a is integer, @param b is integer, @param c is integer, @param d is integer
 *@return the largest of the 4 numbers sent
 */
int max_of_four(int a, int b, int c, int d)
{
    int ans;
    if (a > b&& a > c&& a > d)
        ans = a;
    else if (b > c&& b > d)
        ans = b;
    else if (c > d)
        ans = c;
    else
        ans = d;
    return ans;
}

int main()
{
    int a, b, c, d;
    cin >> a >> b >> c >> d;
    int ans = max_of_four(a, b, c, d);
    cout << ans << endl;
    return 0;
}