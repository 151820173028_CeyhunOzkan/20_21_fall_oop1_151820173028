#include <iostream>
#include <sstream>
using namespace std;
/*!
* @mainpage
* @author 151820173028 , Ceyhun OZKAN ,OOP-1 LAB Homework-2
* @date 09/11/2020
* \brief You can set and get age, first name, last name and standard. You can also access information in one go with the string to_string() method.
*/
/*!
* \class Student
* \brief You can set and get age, first name, last name and standard. You can also access information in one go with the string to_string() method.
*/
class Student {
private:
    int age;
    int standard;
    string first_name;
    string last_name;
public:
    /*!
    * \brief constructor method
    */
    Student()
    {
        age = 0;
        standard = 0;
        first_name.clear();
        last_name.clear();
    }
    /*!
    * \brief Method sets age
    * @param newAge is integer
    * @return nothing
    */
    void set_age(int newAge)
    {
        age = newAge;
    }
    /*!
    * \brief Method sets standard
    * @param newStandard is integer
    * @return nothing
    */
    void set_standard(int newStandard)
    {
        standard = newStandard;
    }
    /*!
    * \brief Method sets first name
    * @param newFirst_name is integer
    * @return nothing
    */
    void set_first_name(string newFirst_name)
    {
        first_name = newFirst_name;
    }
    /*!
    * \brief Method sets last name
    * @param newLast_name is integer
    * @return nothing
    */
    void set_last_name(string newLast_name)
    {
        last_name = newLast_name;
    }
    /*!
    * \brief Method gets age
    * @return age
    */
    int get_age()
    {
        return age;
    }
    /*!
    * \brief Method gets standard
    * @return standard
    */
    int get_standard()
    {
        return standard;
    }
    /*!
    * \brief Method gets first name
    * @return first name
    */
    string get_first_name()
    {
        return first_name;
    }
    /*!
    * \brief Method gets last name
    * @return last name
    */
    string get_last_name()
    {
        return last_name;
    }
    /*!
    * \brief Method gets all informations about student
    * @return all informations about student
    */
    string to_string()
    {
        stringstream ss;
        char comma = ',';
        ss << age << comma << first_name << comma << last_name << comma << standard;
        return ss.str();
    }
};
int main() {
    int age, standard;
    string first_name, last_name;

    cin >> age >> first_name >> last_name >> standard;

    Student st;
    st.set_age(age);
    st.set_standard(standard);
    st.set_first_name(first_name);
    st.set_last_name(last_name);

    cout << st.get_age() << "\n";
    cout << st.get_last_name() << ", " << st.get_first_name() << "\n";
    cout << st.get_standard() << "\n";
    cout << "\n";
    cout << st.to_string();

    return 0;
}
