#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <cassert>
using namespace std;
/*!
* @mainpage
* @author 151820173028 , Ceyhun OZKAN ,OOP-1 LAB Homework-2
* @date 09/11/2020
* \brief It calculates how many people scored higher than someone
*/
/*!
* \class Student
* \brief Includes methods of input and calculateTotalScore
*/
class Student
{
private:
    int points[10];
    int sum;
public:
    /*!
    * \brief constructor method
    */
    Student()
    {
        sum = 0;
    }
    /*!
    * \brief Method calculates sum of points
    * @return nothing
    */
    void input()
    {
        for (int i = 0; i < 5; i++)
        {
            cin >> points[i];
            sum += points[i];
        }
    }
    /*!
    * \brief Method returns sum of points
    * @return sum of points
    */
    int calculateTotalScore()
    {
        return sum;
    }
};

int main() {
    int n; // number of students
    cin >> n;
    Student* s = new Student[n]; // an array of n students

    for (int i = 0; i < n; i++) {
        s[i].input();
    }

    // calculate kristen's score
    int kristen_score = s[0].calculateTotalScore();

    // determine how many students scored higher than kristen
    int count = 0;
    for (int i = 1; i < n; i++) {
        int total = s[i].calculateTotalScore();
        if (total > kristen_score) {
            count++;
        }
    }

    // print result
    cout << count;

    return 0;
}
