//#include<bits/stdc++.h>  Hackerrank'te de�i�tirilemez olarak verilmi�ti.Visual studio uygulamas�nda hata ald���m i�in yorum sat�r� yapt�m.
#include <iostream>
using namespace std;
/*!
* @mainpage
* @author 151820173028 , Ceyhun OZKAN ,OOP-1 LAB Homework-2
* @date 09/11/2020
* \brief It can determine the sides of the box and calculate its volume
/*!
* \class Box
* \brief Includes methods of getLength,getBreadth,getHeight,CalculateVolume
*/
class Box
{
public:
	int a, b, c;
	/*!
	* \brief constructor method
	*/
	Box()
	{
		this->a = 0; this->b = 0; this->c = 0;
	}
	/*!
	* \brief copy constructor method
	* @param length is integer, @param width is integer, @param height is integer,
	*/
	Box(int length, int width, int height)
	{
		this->a = length; this->b = width; this->c = height;
	}
	/*!
	* \brief Method sends length
	* @return length
	*/
	int getLength()
	{
		return this->a;
	}
	/*!
	* \brief Method sends width
	* @return width
	*/
	int getBreadth()
	{
		return this->b;
	}
	/*!
	* \brief Method sends height
	* @return height
	*/
	int getHeight()
	{
		return this->c;
	}
	/*!
	* \brief Method evaluates volume of the box
	* @return volume of the box
	*/
	long long CalculateVolume()
	{
		return (long long)(this->a) * (this->b) * (this->c);
	}
};
/*!
* \brief Bool operator < checks box conditions
* @param a2 is address, @param b2 is address
* @return true or false
*/
bool operator <(Box& a2, Box& b2)
{
	if (a2.a < b2.a)
		return true;
	if (a2.a == b2.a && a2.b < b2.b)
		return true;
	if (a2.a == b2.a && a2.b == b2.b && a2.c < b2.c)
		return true;
	return false;
}
/*!
* \brief ostream& operator << B3's informations sending to out
* @param out is address and @param B3 is address
* @return out
*/
ostream& operator<<(ostream& out, Box& B3)
{
	out << B3.a << " " << B3.b << " " << B3.c;
	return out;
}
/*!
* \brief Function fulfills the desires according to the type
* @return nothing
*/
void check2()
{
	int n;
	cin >> n;
	Box temp;
	for (int i = 0; i < n; i++)
	{
		int type;
		cin >> type;
		if (type == 1)
		{
			cout << temp << endl;
		}
		if (type == 2)
		{
			int l, b, h;
			cin >> l >> b >> h;
			Box NewBox(l, b, h);
			temp = NewBox;
			cout << temp << endl;
		}
		if (type == 3)
		{
			int l, b, h;
			cin >> l >> b >> h;
			Box NewBox(l, b, h);
			if (NewBox < temp)
			{
				cout << "Lesser\n";
			}
			else
			{
				cout << "Greater\n";
			}
		}
		if (type == 4)
		{
			cout << temp.CalculateVolume() << endl;
		}
		if (type == 5)
		{
			Box NewBox(temp);
			cout << NewBox << endl;
		}

	}
}

int main()
{
	check2();
}