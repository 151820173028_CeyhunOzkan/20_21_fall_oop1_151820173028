#include <iostream>
#include <iomanip>
using namespace std;
/*!
   * @mainpage
   * @author 151820173028 , Ceyhun OZKAN ,OOP-1 LAB Homework-2
   * @date 09/11/2020
   * \brief It prints 5 different types of numbers entered
   */
int main()
{
    int a; long b; char c; float d; double e;
    cin >> a >> b >> c >> d >> e;
    cout << a << "\n" << b << "\n" << c << endl;
    cout << fixed << setprecision(3) << d << endl;
    cout << fixed << setprecision(9) << e << endl;
    return 0;
}