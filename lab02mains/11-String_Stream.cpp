#include <sstream>
#include <vector>
#include <iostream>
using namespace std;
/*!
 * @mainpage
 * @author 151820173028 , Ceyhun OZKAN ,OOP-1 LAB Homework-2
 * @date 09/11/2020
 * \brief It converts sent string numbers to integer numbers
 */
 /*!
 * \brief vector<int> It converts sent string numbers to integer numbers
 *@param str is string
 *@return a vector of the parsed integers
 */
vector<int> parseInts(string str)
{
    stringstream ss(str); string s;
    vector<int> res;
    while (getline(ss, s, ','))
        res.push_back(stoi(s));
    return res;
}

int main()
{
    string str;
    cin >> str;
    vector<int> integers = parseInts(str);
    for (int i = 0; i < integers.size(); i++)
        cout << integers[i] << "\n";
    return 0;
}
