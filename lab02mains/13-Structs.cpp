#include <iostream>
using namespace std;
/*!
 * @mainpage
 * @author 151820173028 , Ceyhun OZKAN ,OOP-1 LAB Homework-2
 * @date 09/11/2020
 * \brief It prints age ,first name , last name and standard with class
 */
 /*!
 * \brief Struct prints age ,first name , last name and standard with class
 */
struct Student
{
    int age, standard;
    string first_name;
    string last_name;
};

int main()
{
    Student st;
    cin >> st.age >> st.first_name >> st.last_name >> st.standard;
    cout << st.age << " " << st.first_name << " " << st.last_name << " " << st.standard;
    return 0;
}