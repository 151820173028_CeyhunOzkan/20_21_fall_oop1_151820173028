#include <iostream>
using namespace std;
/*!
   * @mainpage
   * @author 151820173028 , Ceyhun OZKAN ,OOP-1 LAB Homework-2
   * @date 09/11/2020
   * \brief It calculates the sum of three numbers entered
   */
int main() {
    int a, b, c;
    cin >> a >> b >> c;
    cout << a + b + c << endl;
    return 0;
}
