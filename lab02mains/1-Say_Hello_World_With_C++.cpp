#include <iostream>
using namespace std;
/*!
* @mainpage
* @author 151820173028 , Ceyhun OZKAN ,OOP-1 LAB Homework-2
* @date 09/11/2020
* \brief It prints Hello, World!
*/
int main()
{
    cout << "Hello, World!" << endl;
    system("pause");
    return 0;
}