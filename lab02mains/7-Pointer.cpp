#include <iostream>
using namespace std;
/*!
 * @mainpage
 * @author 151820173028 , Ceyhun OZKAN ,OOP-1 LAB Homework-2
 * @date 09/11/2020
 * \brief It evaluates sum of 2 numbers and absolute difference with pointers
 */
 /*!
 * \brief Function finds the largest of the 4 numbers sent
 *@param a is pointer, @param b is pointer
 *@return nothing
 */
void update(int* a, int* b)
{
    int temp = *a;
    *a += *b;
    *b = abs(temp - *b);
}

int main()
{
    int a, b;
    int* pa = &a, * pb = &b;
    cin >> a >> b;
    update(pa, pb);
    cout << a << endl << b << endl;
    return 0;
}