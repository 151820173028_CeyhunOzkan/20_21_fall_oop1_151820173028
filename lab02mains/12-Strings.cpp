#include <iostream>
#include <string>
using namespace std;
/*!
 * @mainpage
 * @author 151820173028 , Ceyhun OZKAN ,OOP-1 LAB Homework-2
 * @date 09/11/2020
 * \brief It prints the lengths, combined versions and interchanged initials of the 2 strings entered
 */
int main()
{
    string a, b; char temp;
    cin >> a >> b;
    cout << a.length() << " " << b.length() << endl;
    cout << a + b << endl;
    temp = a[0];
    a[0] = b[0];
    b[0] = temp;
    cout << a << " " << b << endl;
    return 0;
}