#include <iostream>
using namespace std;
/*!
 * @mainpage
 * @author 151820173028 , Ceyhun OZKAN ,OOP-1 LAB Homework-2
 * @date 09/11/2020
 * \brief If the entered counting number is less than 10, It prints as text if it is greater than 9, It prints greater than 9.
 */
int main()
{
    int number;
    cin >> number;
    string dict[9] = { "one", "two", "three", "four", "five", "six", "seven", "eight","nine" };
    if (number > 9)
        cout << "Greater than 9" << endl;
    else
        cout << dict[number - 1] << endl;
    return 0;
}