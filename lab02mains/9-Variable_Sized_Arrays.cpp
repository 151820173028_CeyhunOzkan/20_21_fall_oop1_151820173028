#include <iostream>
using namespace std;
/*!
 * @mainpage
 * @author 151820173028 , Ceyhun OZKAN ,OOP-1 LAB Homework-2
 * @date 09/11/2020
 * \brief It prints the desired number from indices with different lengths
 */
int main()
{
    int i, j, k, l, m, n, no, col;
    cin >> n >> no;
    int* arr[n];
    for (i = 0; i < n; i++)
    {
        cin >> col;
        arr[i] = new int[col];
        for (j = 0; j < col; j++)
            cin >> arr[i][j];
    }
    for (k = 0; k < no; k++)
    {
        cin >> l >> m;
        cout << arr[l][m] << endl;
    }
    return 0;
}
