#include <iostream>
#include <map>
using namespace std;
/*!
 * @mainpage
 * @author 151820173028 , Ceyhun OZKAN ,OOP-1 LAB Homework-2
 * @date 09/11/2020
 * \brief It prints the value of the attribute for each query.
 */

int main() {
    map<string, string> att;
    int n, q; string s, prefix = "", name; bool cont = true;
    cin >> n >> q;
    for (int i = 0; i < n; i++) {
        do {
            cin >> s;
            if (s[1] != '/' && s[0] == '<') {
                string new_tag = s.substr(1);
                if (new_tag.find('>') != string::npos)
                    new_tag = new_tag.substr(0, new_tag.length() - 1);
                if (!prefix.compare("")) {
                    prefix = new_tag;
                }
                else {
                    prefix += '.';
                    prefix += new_tag;
                }
            }
            else if (s[1] == '/' && s[0] == '<') {
                string end_tag;
                end_tag = s.substr(2, s.length() - 3);
                if (prefix.find('.') != string::npos)
                    prefix.erase(prefix.find(end_tag) - 1);
                else
                    prefix.erase(prefix.find(end_tag));
            }
            else if (s.compare("=")) {
                if (cont) {
                    name = s;
                    cont = false;
                }
                else {
                    string entry = prefix;
                    string val = s.substr(1);
                    val = val.substr(0, val.find('"'));
                    entry += "~";
                    entry += name;
                    att[entry] = val;
                    cont = true;
                }
            }
        } while (s.find(">") == string::npos);
    }
    for (int i = 0; i < q; i++) {
        cin >> s;
        if (!att.count(s))
            cout << "Not Found!";
        else
            cout << att[s];
        cout << '\n';
    }
    return 0;
}