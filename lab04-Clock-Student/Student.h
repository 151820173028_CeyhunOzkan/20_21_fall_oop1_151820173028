#pragma once

#include <string>
using namespace std;



class Student
{
public:
	void setStudent(string _name, long double _id);
	void print();
	double calculateGrade();
	int setmid(int midterm);
	int setfinal(int final);

private:
	string name;
	long double id;
	int midTermExam;
	int finalExam;
};

