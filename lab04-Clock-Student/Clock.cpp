#include "Clock.h"

#include <iostream>
using namespace std;

void CClock::setTime(int hours, int minutes, int seconds)
{
	if (0 <= hours && hours < 24)
		hr = hours;
	else
		hr = 0;

	if (0 <= minutes && minutes < 60)
		min = minutes;
	else
		min = 0;
	
	if (0 <= seconds && seconds < 60)
		sec = seconds;
	else
		sec = 0;
}

void CClock::incrementHours()
{
	hr++;
	if (hr > 23)
		hr=0;
}

void CClock::incrementMinutes()
{
	min++;
	if (min > 59){
		min=0;
		incrementHours(); //increment hours
	}
}
void CClock::incrementSeconds(double a)
{
	sec += a;
	if (sec > 59) {
		sec = 0;
		incrementMinutes(); //increment minutes
	}
}
void CClock::incrementSeconds() const
{
	//sec++;
	if (sec > 59){
		//sec=0;
		//incrementMinutes(); //increment minutes
	}
}

bool CClock::equalTime(const CClock& otherClock) const
{
	return (hr == otherClock.hr
		&& min == otherClock.min
		&& sec == otherClock.sec);

}

void CClock::printTime()const
{
	if (hr <10)
		cout << "0";
	cout << hr <<":";
	if (min <10)
		cout << "0";
	cout << min <<":";
	if (sec <10)
		cout << "0";
	cout << sec ;
}
bool operator== (const CClock& c1, const CClock& c2)
{
	if (c1.hr == c2.hr && c1.min == c2.sec && c1.sec == c2.sec)
		return true;
	else
		return false;
}


