/*
 * Square.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */

#include <iostream>
using namespace std;
#include "Square.h"
/*!
* \brief constructor method
* @param a is double.
*/
Square::Square(double a) {
	setA(a);
	setB(a);
}
/*!
* \brief destructor method
*/
Square::~Square() {
}
/*!
* \brief Method sets edges of square.
* @param a is double.
* @return nothing.
*/
void Square::setA(double a) {
	this->a = a;
	this->b = a;
}
/*!
* \brief Method sets edges of square.
* @param b is double.
* @return nothing.
*/
void Square::setB(double b) {
	this->b = b;
	this->a = b;
}
/*!
* \brief Method evaluates circumference of square.
* @return circumference of square.
*/
double Square::calculateCircumference() {
	return (a + b) * 2;
}
/*!
* \brief Method evaluates area of square.
* @return area of square.
*/
double Square::calculateArea() {
	return a * b;
}

