/*
 * Triangle.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */

#include <iostream>
using namespace std;
#include "Triangle.h"
/*!
* \brief constructor method
* @param a , b , c are double.
*/
Triangle::Triangle(double a, double b, double c) {
	setA(a);
	setB(b);
	setC(c);
}
/*!
* \brief destructor method
*/
Triangle::~Triangle() {

}
/*!
* \brief Method sets an edge of triangle.
* @param a is double.
* @return nothing.
*/
void Triangle::setA(double a) {
	this->a = a;
}
/*!
* \brief Method sets an edge of triangle.
* @param b is double.
* @return nothing.
*/
void Triangle::setB(double b) {
	this->b = b;
}
/*!
* \brief Method sets an edge of triangle.
* @param c is double.
* @return nothing.
*/
void Triangle::setC(double c) {
	this->c = c;
}
/*!
* \brief Method evaluates circumference of triangle.
* @return circumference of triangle.
*/
double Triangle::calculateCircumference() {
	return a + b + c;
}




