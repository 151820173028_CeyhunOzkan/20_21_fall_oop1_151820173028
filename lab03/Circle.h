/*
 * Circle.h
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */


#ifndef CIRCLE_H_
#define CIRCLE_H_
/*!
* \class Circle
* \brief Includes methods of Circle(double),
*	virtual ~Circle(),
*	void setR(double),
*	void setR(int),
*	double getR()const,
*	double calculateCircumference()const,
*	double calculateArea()const,
*	friend bool operator==(const Circle &c1,const Circle &c2).
*/
class Circle {
public:
	Circle(double);
	virtual ~Circle();
	void setR(double);
	void setR(int);
	double getR()const;
	double calculateCircumference()const;
	double calculateArea()const;
	friend bool operator==(const Circle &c1,const Circle &c2);
private:
	double r;
	const double PI = 22.0/7; 
};
#endif /* CIRCLE_H_ */
