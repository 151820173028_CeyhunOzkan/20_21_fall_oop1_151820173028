/*
 * Circle.cpp
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */

#include <iostream>
using namespace std;
#include "Circle.h"

/*!
* \brief constructor method
* @param r is double.
*/
Circle::Circle(double r) {
	setR(r);
}
/*!
* \brief destructor method
*/
Circle::~Circle() {
}
/*!
* \brief Method sets radius of circle.
* @param r is double.
* @return nothing.
*/
void Circle::setR(double r) {
	this->r = r;
}
/*!
* \brief Method sets radius of circle.
* @param r is integer.
* @return nothing.
*/
void Circle::setR(int r) {
	this->r = r;
}
/*!
* \brief Method gets radius of circle.
* @return radius of circle.
*/
double Circle::getR()const {
	return r;
}
/*!
* \brief Method gets Circumference of circle.
* @return Circumference of circle.
*/
double Circle::calculateCircumference()const {
	return PI * r * 2;
}
/*!
* \brief Method gets area of circle.
* @return area of circle.
*/
double Circle::calculateArea()const {
	return PI * r * r;
}
/*!
* \brief Method checks two objects are equal or not.
* @return if two objects are equal 1, if not 0. 
*/
bool operator== (const Circle& c1, const Circle& c2)
{
	return c1.r == c2.r;
}