/*!
* @mainpage
* @author 151820173028 , Ceyhun OZKAN ,OOP-1 LAB Homework-1
*/
#ifndef COLOR_HPP_INCLUDED
#define COLOR_HPP_INCLUDED
#include <iostream>
#include <math.h>
#include <fstream>
#include <windows.h>
using namespace std;

bool textcolorprotect = true;
/* doesn't let textcolor be the same as background color if true */
/*
* \brief contains color codes
*/
enum colors
{
	black = 0,
	dark_blue = 1,
	dark_green = 2,
	dark_aqua = 3, dark_cyan = 3,
	dark_red = 4,
	dark_purple = 5, dark_pink = 5, dark_magenta = 5,
	dark_yellow = 6,
	dark_white = 7,
	gray = 8,
	blue = 9,
	green = 10,
	aqua = 11, cyan = 11,
	red = 12,
	purple = 13, pink = 13, magenta = 13,
	yellow = 14,
	white = 15
};

inline void setcolor(colors textcolor, colors backcolor);
inline void setcolor(int textcolor, int backcolor);
int textcolor(); /* returns current text color */
int backcolor(); /* returns current background color */

#define std_con_out GetStdHandle(STD_OUTPUT_HANDLE)

int textcolor()
{
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	GetConsoleScreenBufferInfo(std_con_out, &csbi);
	int a = csbi.wAttributes;
	return a % 16;
}

int backcolor()
{
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	GetConsoleScreenBufferInfo(std_con_out, &csbi);
	int a = csbi.wAttributes;
	return (a / 16) % 16;
}
/*
* \brief It takes text color and backcolor and sets them
* @param textcolor is colors variable and @param backcolor is also colors variable
* @return nothing
*/
inline void setcolor(colors textcol, colors backcol)
{
	setcolor(int(textcol), int(backcol));
}
/*
* \brief It takes text color and backcolor and sets them
* @param textcolor is int variable and @param backcolor is also int variable
* @return nothing
*/
inline void setcolor(int textcol, int backcol)
{
	if (textcolorprotect)
	{
		if ((textcol % 16) == (backcol % 16))textcol++;
	}

	textcol %= 16; backcol %= 16;
	unsigned short wAttributes = ((unsigned)backcol << 4) | (unsigned)textcol;
	SetConsoleTextAttribute(std_con_out, wAttributes);
}
//#if defined(_INC_OSTREAM)||defined(_IOSTREAM_)
std::ostream& operator<<(std::ostream& os, colors c)
{
	os.flush(); setcolor(c, backcolor()); return os;
}
//#endif

//#if defined(_INC_ISTREAM)||defined(_IOSTREAM_)
std::istream& operator>>(std::istream& is, colors c)
{
	std::cout.flush(); setcolor(c, backcolor()); return is;
}
//#endif 

#endif // COLOR_HPP_INCLUDED 

/*!
* \class lab01
* \brief Includes methods of addition, multiplication, average calculation and finding the smallest number
*/
class lab01
{
public:
	/*!
	* \brief constructor method
	*/
	lab01() {};
	/*!
	* \brief destructor method
	*/
	~lab01() {};
	/*!
	* \brief Method evaluates the sum of the elements in the array according to the sent array and array size.
	* @param arr[100] is double array variable and @param size is int variable.
	* @return sum of the elements in the array
	*/
	double Sum(double arr[100], int size)
	{
		double sum = 0; //sum variable 
		// It calculates sum of the elements
		for (int i = 0; i < size; i++)
			sum += arr[i];
		return sum;
	}
	/*!
	* \brief Method evaluates the product of the elements in the array according to the sent array and array size.
	* @param arr[100] is double array variable and @param size is int variable.
	* @return product of the elements in the array
	*/
	double Product(double arr[100], int size)
	{
		double pro = 1; // product variable 
		// It calculates product of the elements
		for (int i = 0; i < size; i++)
			pro *= arr[i];
		if (pro == 0)
			return abs(pro);
		return pro;
	}
	/*!
	* \brief Method evaluates the average of the elements in the array according to the sent array and array size.
	*@param arr[100] is double array variable and @param size is int variable.
	*@return average of the elements in the array
	*/
	double Average(double arr[100], int size)
	{
		double sum = 0; // sum variable 
		// It calculates sum of the elements
		for (int i = 0; i < size; i++)
			sum += arr[i];
		return sum / size;
	}
	/*!
	* \brief Method finds the smallest element in the array, based on the array and array size sent.
	*@param arr[100] is double array variable and @param size is int variable.
	*@return smallest element in the array
	*/
	double Smallest(double arr[100], int size)
	{
		double min = arr[0]; //minimum variable 
		// It finds minimum element
		for (int i = 0; i < size; i++)
		{
			if (arr[i] < min)
				min = arr[i];
		}
		return min;
	}
};

/*!
* \brief main part
* @return nothing
*/
void main()
{
	int size; //size of array variable 
	double storage[100]; // array variable 
	fstream datafile; // fstream variable 
	char filenamev2[100]; // filename variable 
	lab01 obj; // object of the class lab01 

	cout << yellow << "Enter file name with extension to open: ";
	cin >> green >> filenamev2;

	datafile.open(filenamev2, ios::in);
	// checks if the file exists
	if (!datafile)
	{
		cout << "Error! File can not be opened!";
		exit(0);
	}
	// size is equals to first element of the datafile
	datafile >> size;
	// datafile is being transferred to the storage array
	for (int i = 0; i < size; i++)
	{
		datafile >> storage[i];
	}
	double x = obj.Sum(storage, size); // x is equal to the sum of the elements in the array 
	// prints x , It means sum of the elements in the array
	cout << magenta << "Sum is " << aqua << x << endl;
	double y = obj.Product(storage, size); // y is equal to the product of the elements in the array 
	// prints y, It means product of the elements in the array
	cout << magenta << "Product is " << aqua << y << endl;
	double z = obj.Average(storage, size); // z is equal to the average of the elements in the array 
	// prints z , It means average of the elements in the array
	cout << magenta << "Average is " << aqua << z << endl;
	double q = obj.Smallest(storage, size); // q is equal to the smallest element in the array 
	// prints q, It means smallest element in the array
	cout << magenta << "Smallest is " << aqua << q << endl << yellow;
	system("pause");
}